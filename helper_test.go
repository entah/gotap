package gotap

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_removeAt(t *testing.T) {
	fna := func(i interface{}) {}
	fnb := func(i interface{}) {}
	fnc := func(i interface{}) {}
	// fnd := func(i interface{}) {}
	// fne := func(i interface{}) {}

	type args struct {
		list []*callable
		i    int
	}
	tests := []struct {
		name      string
		args      args
		validator func([]*callable) bool
	}{
		{
			"1",
			args{list: []*callable{}, i: 2},
			func(c []*callable) bool {
				return len(c) == 0
			},
		},
		{
			"2",
			args{list: []*callable{{fna, "fna"}}, i: 0},
			func(c []*callable) bool {
				return len(c) == 0
			},
		},
		{
			"3",
			args{list: []*callable{{fna, "fna"}}, i: 2},
			func(c []*callable) bool {
				first := c[0]
				return len(c) == 1 && first.ident == "fna"
			},
		},
		{
			"4",
			args{list: []*callable{{fna, "fna"}, {fnb, "fnb"}}, i: 1},
			func(c []*callable) bool {
				first := c[0]
				return len(c) == 1 && first.ident == "fna"
			},
		},
		{
			"5",
			args{list: []*callable{{fna, "fna"}, {fnb, "fnb"}}, i: 0},
			func(c []*callable) bool {
				first := c[0]
				return len(c) == 1 && first.ident == "fnb"
			},
		},
		{
			"6",
			args{list: []*callable{{fna, "fna"}, {fnb, "fnb"}, {fnc, "fnc"}}, i: 1},
			func(c []*callable) bool {
				first := c[0]
				second := c[1]
				return len(c) == 2 && first.ident == "fna" && second.ident == "fnc"
			},
		},
	}
	for _, tt := range tests {
		assert.True(t, tt.validator(removeAt(tt.args.list, tt.args.i)), tt.name)
	}
}

func Test_parseStringInt(t *testing.T) {
	type args struct {
		str    string
		defVal int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "1",
			args: args{"", 1},
			want: 1,
		},
		{
			name: "2",
			args: args{"foo", 10},
			want: 10,
		},
		{
			name: "3",
			args: args{"100", 1000},
			want: 100,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseStringInt(tt.args.str, tt.args.defVal); got != tt.want {
				t.Errorf("parseStringInt() = %v, want %v", got, tt.want)
			}
		})
	}
}
