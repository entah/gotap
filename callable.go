package gotap

import (
	"regexp"
)

// callable to hold out func and it's ident.
type callable struct {
	fn    func(interface{})
	ident string
}

// callableContainer will serve request to access list of receiver func.
type callableContainer struct {
	accessList []*regexp.Regexp
	accessMode string
	list       []*callable
	chanList   chan chan<- []*callable
	chanAttach chan func(interface{})
	chanDetach chan func(interface{})
}

// serve request to access list of receiver func.
func (cc *callableContainer) serve() {
	for {
		select {

		case fn := <-cc.chanAttach:
			ident := getFuncInfo(fn)
			if cc.grantAttach(ident) {
				pos := getPos(cc.list, ident)
				if pos == -1 {
					cc.list = append(cc.list, &callable{fn: fn, ident: ident})
				}
			}

		case fn := <-cc.chanDetach:
			ident := getFuncInfo(fn)
			pos := getPos(cc.list, ident)
			if pos > -1 {
				cc.list = removeAt(cc.list, pos)
			}

		case chanReq := <-cc.chanList:
			chanReq <- cc.list
			close(chanReq)
		}
	}
}

// grantAttach granting attach based on `GOTAP_ACCESS_LIST` env var.
// if `GOTAP_ACCESS_LIST` not set, `grantAttach` will always return true.
// see var `accessList` and `accessMode`.
func (cc *callableContainer) grantAttach(ident string) bool {
	if len(cc.accessList) < 1 {
		return true
	}

	for _, re := range cc.accessList {
		match := re.MatchString(ident)

		if match {
			return cc.accessMode == "whitelist"
		}
	}

	return cc.accessMode == "blacklist"
}

// getList get list of receiver func.
func (cc *callableContainer) getList(chanReq chan<- []*callable) {
	cc.chanList <- chanReq
}

// attachOrDetach if `ops` == 1 then do attach or `ops` == 0 then do detach receiver func.
func (cc *callableContainer) attachOrDetach(ops int, fn func(interface{})) {
	switch ops {
	case 1:
		cc.chanAttach <- fn
	case 0:
		cc.chanDetach <- fn
	}
}

// invoke call a func with argument `arg` and read `done` chan upon returning.
func (c *callable) invoke(done <-chan int, arg interface{}) {
	defer func() {
		<-done
	}()
	defer recoverAndPrintLog()
	c.fn(arg)
}

// // getIdent return comparabel and unique identity of out func.
// func (c *callable) getIdent() interface{} {
// 	return c.ident
// }
