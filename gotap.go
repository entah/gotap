// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

// Package gotap stream anything to anywhere.
package gotap

import (
	"os"
)

// callables list of out func that will be our consumer.
var callables *callableContainer

// mainChan buffered chan with capacity of 10.
var mainChan chan interface{}

// maxBuffer maximum buffer for `gitlab.com/entah/gotap#mainChan` channel's buffer.
// This buffer can be overrides by env `GOTAP_MAX_BUFFER` upon `init` phase.
var maxBuffer = 10

// envMaxBuffer environment variable `GOTAP_MAX_BUFFER` to override value of `gitlab.com/entah/gotap#maxBuffer`.
var envMaxBuffer = os.Getenv("GOTAP_MAX_BUFFER")

// accessMode environment variable `GOTAP_ACCESS_MODE` to set mode of access control.
// The value either `blacklist` or `whitelist`, default to `whitelist`
var accessMode = strOr(os.Getenv("GOTAP_ACCESS_MODE"), "whitelist")

// accessList environment variable `GOTAP_ACCESS_LIST` to grant access to the set of attached func.
// See var `accessMode` for available mode.
// If this environment variable not set, will grant all func.
// The value must be regexp and separated by semicolon for multi value eg: `entah/gotap;entah/fngo`
var accessList = os.Getenv("GOTAP_ACCESS_LIST")

// reqsChan chan to capping number go routine to invoke out func.
var reqsChan = make(chan int, 10)

// Attach out func `fn` to consume event data.
func Attach(fn func(arg interface{})) {
	callables.attachOrDetach(1, fn)
}

// Detach out func registered by `gitlab.com/entah/gotap#Attach`.
func Detach(fn func(arg interface{})) {
	callables.attachOrDetach(0, fn)
}

// ListAttached list attached unique identifier of out func.
func ListAttached() []interface{} {
	chanReq := make(chan []*callable, 1)
	callables.getList(chanReq)

	res := []interface{}{}

	for _, call := range <-chanReq {
		res = append(res, call.ident)
	}
	return res
}

// Tap anything to channel.
func Tap(thing interface{}) {
	mainChan <- thing
}

// receive receiver end to apply value get from channel and apply to list of out func.
func receive() {
	for val := range mainChan {

		chanReq := make(chan []*callable, 1)
		callables.getList(chanReq)

		for i, callable := range <-chanReq {
			reqsChan <- i
			go callable.invoke(reqsChan, val)
		}
	}
}

func init() {
	defer recoverAndPrintLog()

	alist := parseAList(accessList)

	callables = &callableContainer{
		accessMode: accessMode,
		accessList: alist,
		chanList:   make(chan chan<- []*callable, 1),
		chanAttach: make(chan func(interface{}), 1),
		chanDetach: make(chan func(interface{}), 1),
	}

	go callables.serve()

	maxBuffer = parseStringInt(envMaxBuffer, maxBuffer)
	mainChan = make(chan interface{}, maxBuffer)

	go receive()
}
