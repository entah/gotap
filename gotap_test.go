package gotap

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

type mutInt struct {
	val  int
	lock sync.Mutex
}

type mutStr struct {
	val  string
	lock sync.Mutex
}

func (mi *mutInt) Get() int {
	mi.lock.Lock()
	defer mi.lock.Unlock()
	return mi.val
}

func (ms *mutStr) Get() string {
	ms.lock.Lock()
	defer ms.lock.Unlock()
	return ms.val
}

func fnInt(holder *mutInt) func(i interface{}) {
	return func(i interface{}) {
		if arg, ok := i.(int); ok {
			holder.lock.Lock()
			defer holder.lock.Unlock()
			holder.val += (arg + 2)
		}
	}
}

func fnStr(holder *mutStr) func(i interface{}) {
	return func(i interface{}) {
		if arg, ok := i.(string); ok {
			holder.lock.Lock()
			defer holder.lock.Unlock()
			holder.val += arg
		}
	}
}

func fnNotNil(holder *mutInt) func(i interface{}) {
	return func(i interface{}) {
		if i != nil {
			holder.lock.Lock()
			defer holder.lock.Unlock()
			holder.val++
		}
	}
}

func TestGotap(t *testing.T) {

	// assert.True(t, maxBuffer == nil)

	// nothing happen if we don't have any out func registered.
	Tap(0.99)
	Tap(struct{ Name string }{})

	assert.Empty(t, ListAttached())

	intHolder := &mutInt{}
	strHolder := &mutStr{}
	counter := &mutInt{}

	fnIntHolder := fnInt(intHolder)
	fnStrHolder := fnStr(strHolder)
	fnCounter := fnNotNil(counter)

	worker := func(wg *sync.WaitGroup, val interface{}) {
		defer wg.Done()
		Tap(val)
	}

	// let's register our out func
	Attach(fnIntHolder)
	Attach(fnStrHolder)
	Attach(fnCounter)
	assert.Equal(t, 3, len(ListAttached()))

	// register the same ident, will result noop
	go Attach(fnIntHolder)
	go Attach(fnStrHolder)
	go Attach(fnCounter)
	assert.Equal(t, 3, len(ListAttached()))

	// create wait group to wait all our go routine to finish
	wg := &sync.WaitGroup{}

	for i := 0; i < 1000; i++ {
		wg.Add(3)
		go worker(wg, i)
		go worker(wg, "a")
		go worker(wg, nil)
	}

	wg.Wait()
	assert.True(t, intHolder.Get() > 125749)
	assert.True(t, len(strHolder.Get()) > 200)
	assert.True(t, counter.Get() > 500)

	// let register out func that will be panic
	fnPanic := func(arg interface{}) { panic("panic") }
	Attach(fnPanic)
	assert.Equal(t, 4, len(ListAttached()))
	Tap(nil)

	assert.NotPanics(t, func() {
		defer recoverAndPrintLog()
		panic("panic")
	})

	// let unregister anything
	Detach(fnPanic)
	Detach(fnIntHolder)
	Detach(fnStrHolder)
	Detach(fnCounter)
	assert.Empty(t, ListAttached())

	// unregister unknown ident is noop operation
	Detach(func(arg interface{}) {})
	Tap("foo")
}
