// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package gotap

import (
	"log"
	"reflect"
	"regexp"
	"runtime"
	"strconv"
	"strings"
)

func parseAList(accessList string) []*regexp.Regexp {
	var res []*regexp.Regexp
	list := strings.Split(accessList, ";")
	for _, l := range list {
		res = append(res, regexp.MustCompile(strings.TrimSpace(l)))
	}
	return res
}

func strOr(str1, str2 string) string {
	if str1 == "" {
		return str2
	}
	return str1
}

func recoverAndPrintLog() {
	if r := recover(); r != nil {
		log.Println("fail on call: ", r)
	}
}

// removeAt remove `gitlab.com/entah/gotap#callable` in `list` at index `i`.
func removeAt(list []*callable, i int) []*callable {
	lenList := len(list)

	switch {
	case lenList < 1 || i > lenList-1:
		return list
	case i == 0 && lenList > 0:
		return list[i+1:]
	case lenList-1 == i:
		return list[:lenList-1]
	default:
		return append(list[:i], list[i+1:]...)
	}
}

// getPos get position of `ident` from `list`.
func getPos(list []*callable, ident string) int {
	for i, c := range list {
		if c.ident == ident {
			return i
		}
	}
	return -1
}

// getFuncInfo get func info from `fn`.
func getFuncInfo(fn interface{}) string {
	rfn := runtime.FuncForPC(reflect.ValueOf(fn).Pointer())
	splittedFnName := strings.Split(rfn.Name(), "/")
	fnName := splittedFnName[len(splittedFnName)-1]
	pkgPath := strings.Join(append(splittedFnName[0:len(splittedFnName)-1], strings.Split(fnName, ".")[0]), "/")
	return pkgPath + "#" + fnName
}

// parseStringInt parse string int `str`, if error return `defVal`.
func parseStringInt(str string, defVal int) int {
	parsed, err := strconv.Atoi(str)
	if err != nil {
		return defVal
	}
	return parsed
}
